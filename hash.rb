#hash
 numbers = {a:1, b:2, c:3, d:4, e:5, f:6}
 puts numbers

#adding elements on hash

numbers[:g] = 7
puts numbers

#removing elements from hash

numbers.delete(:c)

#to iterate through hash using each method and printing value

numbers.each{|k, v| puts v}

#to iterate and delete item from hash based on condition

puts numbers.each{|k, v| numbers.delete(k) if v>3}

#using select method to display value of items if they are odd

puts numbers.select{|k, v| v.odd? }