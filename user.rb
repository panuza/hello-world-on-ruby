module Destructable
	def destroy(anyobject)
		puts "I will destroy the object"
	end
end


class User
	include Destructable
	attr_accessor :name, :email

	def initialize(name, email)
		@name = name
		@email = email
	end

	def run
		puts "i am running"

	end

	
end

class Buyer < User
	def run
		puts "i am not running and m in buyer class"

	end


end

class Seller < User


end

class Admin < User


end


user = User.new("panu", "panu@example.com")
user.destroy("my name")

puts "my name is #{user.name} and email is #{user.email}"


user.name = "john"
user.email = "john@example.com"

puts "my new name is #{user.name} and email is #{user.email}"

buyer1 = Buyer.new("rony", "rony@example.com")
buyer1.run

seller1 = Seller.new("monu", "m@example.com")
seller1.run

admin1 = Admin.new("nina", "nn@example.com")
admin1.run
