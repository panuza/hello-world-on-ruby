dial_book = {
	"chitwan" => "212",
	"itahari" => "214",
	"dharan" => "217",
	"biratnagar" => "345",
	"kathmandu" => "112",
	"bhaktpur" => "102",
	"pokhera" => "512",
	"bharatpur" => "242",
	"birtamod" => "218",
	"gorkha" => "612"
}


def get_city_names(somehash)
	somehash.each{|k, v| puts k}
end

def get_area_code(somehash, key)
	somehash[key]
end

loop  do
	puts "Do you want to look up area code based on city name?(Y/N)"

	answer = gets.chomp

	if answer != "Y"
		break
	end
	
	puts "which city do you want the area code for?"
	get_city_names(dial_book)
	puts "enter your selection"
	prompt = gets.chomp

	if dial_book.include?(prompt)
		puts "The area code for #{prompt} is #{get_area_code(dial_book, prompt)}"
	else
		puts "You entered a city name that is not available in dictionary"
	end

end